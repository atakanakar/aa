package com.example.sample.model

data class User(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)