package com.example.sample.ui.login

import androidx.lifecycle.ViewModel

class LoginViewModel: ViewModel() {

    var username: String? = null
    var password: String? = null
    var isLoginSuccess = false
    var toastText = ""


    fun toastMessage(): String {
        return toastText
    }

    fun loginClick() {
        if (!username.isNullOrEmpty() && !password.isNullOrEmpty()) {
            if (username == "1" && password == "1") {
                isLoginSuccess = true
                toastText = "Login: 'Success'"
            } else {
                isLoginSuccess = false
                toastText = "Wrong e-mail or password"
            }
        } else {
            isLoginSuccess = false
            toastText = "Fields can not be empty"
        }
    }
}