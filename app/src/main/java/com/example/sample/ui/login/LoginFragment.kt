package com.example.sample.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.sample.base.BaseFragment
import com.example.sample.R
import com.example.sample.constants.Constant
import com.example.sample.databinding.FragmentLoginBinding

class LoginFragment : BaseFragment() {

    lateinit var viewModel: LoginViewModel
    lateinit var binding: FragmentLoginBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getMainActivity()!!.setupToolbar(Constant.LOGIN_TOOLBAR_TITLE)

        binding.handler = viewModel
        binding.fragmentHandler = this

        binding.nameEt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.username = s.toString()
            }
        })
        binding.passEt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.password = s.toString()
            }
        })

        hideLeftButton()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_login, container, false
        )
        binding.lifecycleOwner = this
        return binding.root
    }


    fun clickNavigate() {
        viewModel.loginClick()
        Toast.makeText(activity!!.baseContext, viewModel.toastMessage(), Toast.LENGTH_SHORT).show()

        if (viewModel.isLoginSuccess) {
            findNavController().navigate(R.id.action_loginFragment_to_listFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.isLoginSuccess = false
    }
}




