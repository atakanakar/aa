package com.example.sample.ui.userlist.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.sample.R
import com.example.sample.constants.Constant
import com.example.sample.model.User
import kotlinx.android.synthetic.main.item_user.view.*

class UserAdapter : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    var users : ArrayList<User> = ArrayList()


    class UserViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user,parent,false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if (!users.isNullOrEmpty()) {
            users.size
        } else {
            0
        }
    }
    fun addToAllList(userList: List<User> ) {
        users.addAll(userList)
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val item = users[position]

        val id = item.userId.toString()
        val title = item.title
        val description =  item.body


        holder.itemView.userId.text = id
        holder.itemView.title_tv.text = title
        holder.itemView.description_tv.text = description

        holder.itemView.userItemCardVİew.setOnClickListener{
            Bundle().apply {
                putString(Constant.bundle_userId, id)
                putString(Constant.bundle_title, title)
                putString(Constant.bundle_description, description)
                Navigation.findNavController(it).navigate(R.id.action_listFragment_to_userDetailFragment,this)
            }
        }
    }
}