package com.example.sample.ui.userdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.sample.R
import com.example.sample.base.BaseFragment
import com.example.sample.constants.Constant
import com.example.sample.databinding.FragmentUserDetailBinding

class UserDetailFragment: BaseFragment() {

    lateinit var viewModel: UserDetailViewModel
    lateinit var binding: FragmentUserDetailBinding

    var userId: String? = null
    var title: String? = null
    var description: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMainActivity().setupToolbar(Constant.DETAIL_TOOLBAR_TITLE)
        showLeftButton()
        binding.detailHandler = viewModel

        userId       = arguments?.getString(Constant.bundle_userId)
        title        = arguments?.getString(Constant.bundle_title)
        description  = arguments?.getString(Constant.bundle_description)

        viewModel.itemUserId      = userId
        viewModel.itemTitle       = title
        viewModel.itemDescription = description

        setClickListeners()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UserDetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_detail,container,false)
        binding.lifecycleOwner = this
        return binding.root
    }
    private fun setClickListeners() {
        getMainActivity().mToolbarLeftButton.setOnClickListener{
            activity?.onBackPressed()
        }
    }
}