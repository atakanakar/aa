package com.example.sample.ui.userlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sample.model.User
import com.example.sample.network.ManagerAll
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UserListViewModel : ViewModel() {

    var myUserList: List<User> = ArrayList()

    val liveDataOnFailure =  MutableLiveData<String>()
    val liveDataCallList = MutableLiveData<List<User>>()

    private val manager by lazy { ManagerAll() }

    fun sendRequestForList() {
        val allUsers: Call<List<User>> = manager.getUserList()
        allUsers.enqueue(object : Callback<List<User>>{
            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                liveDataOnFailure.postValue(t.localizedMessage)
            }

            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                if (response.isSuccessful){
                    myUserList = response.body()!!
                    liveDataCallList.postValue(myUserList)
                }
            }
        })
    }

}