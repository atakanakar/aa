package com.example.sample.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.example.sample.R
import com.example.sample.R.id.mainToolbar
import kotlinx.android.synthetic.main.toolbar_home.*

class MainActivity : AppCompatActivity() {

    lateinit var mToolbarLeftButton: ImageButton
    lateinit var mToolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mToolbarLeftButton = findViewById(R.id.leftButton)
        mToolbarTitle      = findViewById(R.id.toolbar_title)

    }

    open fun setupToolbar(title: String) {
        mToolbarTitle.text = title
    }
}
