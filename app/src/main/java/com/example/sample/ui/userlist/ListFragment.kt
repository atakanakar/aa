package com.example.sample.ui.userlist


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sample.base.BaseFragment
import com.example.sample.R
import com.example.sample.constants.Constant
import com.example.sample.ui.userlist.adapter.UserAdapter
import com.example.sample.databinding.FragmentListBinding
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : BaseFragment() {

    lateinit var myAdapter: UserAdapter

    lateinit var viewModel: UserListViewModel
    lateinit var binding: FragmentListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UserListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list,container,false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMainActivity().setupToolbar(Constant.LIST_TOOLBAR_TITLE)

        binding.listHandler = viewModel
        viewModel.sendRequestForList()

        initRecyclerView()
        liveDataObservers()
        showLeftButton()
        setClickListeners()

    }


    private fun initRecyclerView() {
        rv.layoutManager = LinearLayoutManager(activity!!.baseContext)
        rv.setHasFixedSize(true)
        myAdapter = UserAdapter()
        rv.adapter = myAdapter
    }
    private fun liveDataObservers() {
        viewModel.liveDataOnFailure.observe(viewLifecycleOwner, Observer {
            Toast.makeText(activity!!.baseContext,it,Toast.LENGTH_LONG).show()
        })
        viewModel.liveDataCallList.observe(viewLifecycleOwner, Observer {
            myAdapter.addToAllList(it)
        })
    }

    private fun setClickListeners() {
        getMainActivity().mToolbarLeftButton.setOnClickListener{
            activity?.onBackPressed()
        }
    }

}
