package com.example.sample.base

import android.view.View
import androidx.fragment.app.Fragment
import com.example.sample.ui.main.MainActivity

abstract class BaseFragment: Fragment() {

    open fun getMainActivity(): MainActivity {
        return activity as MainActivity
    }
    open fun hideLeftButton() {
        getMainActivity().mToolbarLeftButton.visibility = View.GONE
    }
    open fun showLeftButton() {
        getMainActivity().mToolbarLeftButton.visibility = View.VISIBLE
    }
}
