package com.example.sample.constants

object Constant {

    const val bundle_userId         = "userId"
    const val bundle_title          = "title"
    const val bundle_description    = "description"

    const val OK_HTTP_TIMEOUT_30    = 30L
    const val OK_HTTP_TIMEOUT_3     = 3L

    const val LOGIN_TOOLBAR_TITLE   = "Login"
    const val LIST_TOOLBAR_TITLE    = "Users"
    const val DETAIL_TOOLBAR_TITLE  = "User Detail"

}