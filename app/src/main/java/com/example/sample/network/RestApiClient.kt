package com.example.sample.network

import com.example.sample.constants.Constant
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RestApiClient(baseUrl: String) {
    private val restApiListener: RestApi
    init {
        val builder = OkHttpClient.Builder()
            .readTimeout(Constant.OK_HTTP_TIMEOUT_30, TimeUnit.SECONDS)
            .writeTimeout(Constant.OK_HTTP_TIMEOUT_30, TimeUnit.SECONDS)
            .connectTimeout(Constant.OK_HTTP_TIMEOUT_3, TimeUnit.SECONDS)
        val client = builder.build()

        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        restApiListener = retrofit.create(RestApi::class.java)
    }

    fun getRestApiClient(): RestApi {
        return restApiListener
    }
}