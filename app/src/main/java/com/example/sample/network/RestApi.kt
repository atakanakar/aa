package com.example.sample.network

import com.example.sample.model.User
import retrofit2.Call
import retrofit2.http.GET

interface RestApi {
    @GET(API.GET_ALL_USERS)
    fun callAdvertList(): Call<List<User>>
}