package com.example.sample.network

import com.example.sample.model.User
import retrofit2.Call

class ManagerAll : BaseManager() {
    fun getUserList(): Call<List<User>> {
        return BaseManager().restApiClient.callAdvertList()
    }
}